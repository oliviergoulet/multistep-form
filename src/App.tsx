import MultiStepForm from './MultiStepForm';
import Step1 from './steps/Step1';
import Step2 from './steps/Step2';
import * as yup from 'yup';
import FormStep from './FormStep';

const initialValues = {
  0: {
    name: '',
    age: 0,
  },
  1: { signed: false },
  2: {
    name: '',
    age: 0,
  },
};

const validationSchema = yup.object().shape({
  0: yup.object().shape({
    name: yup.string().required(),
    age: yup.number().min(18).max(99).required(),
  }),
  1: yup.object().shape({
    signed: yup.boolean().required()
  }),
  2: yup.object().shape({
    name: yup.string().required(),
    age: yup.number().min(18).max(99).required(),
  }),
});

function App() {
  return (
    <MultiStepForm initialValues={initialValues} onSubmit={() => console.log('Submitted All')} validationSchema={validationSchema}>
      <FormStep onSubmit={() => console.log('Submitted 1')}>
        <Step1 />
      </FormStep>
      <FormStep onSubmit={() => console.log('Submitted 2')}>
        <Step2 />
      </FormStep>
      <FormStep onSubmit={() => console.log('Submitted 3')}>
        <Step1 />
      </FormStep>
    </MultiStepForm>
  )
}

export default App;

