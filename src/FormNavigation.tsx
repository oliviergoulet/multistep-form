import React, { useContext } from 'react';
import { MultiStepFormContext } from './MultiStepForm';
import { useFormikContext, FormikValues } from 'formik';

const FormNavigation: React.FC = () => {
  const { values } = useFormikContext();
  const { hasNext, prev, hasPrev } = useContext(MultiStepFormContext);
  return (
    <div>
      <button type="button" onClick={() => prev(values as FormikValues)} disabled={hasPrev}>Back</button>
      <button type="submit">
        { hasNext ? 'Next' : 'Submit' }
      </button>
    </div>
  );
};

export default FormNavigation;
