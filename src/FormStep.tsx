import { useFormikContext, Form } from 'formik';
import FormNavigation from './FormNavigation';

const FormStep = ({ children }: any) => {
  const { handleSubmit } = useFormikContext();
  return (
    <Form onSubmit={handleSubmit}>
      {children}
      <FormNavigation />
    </Form>
  )
}

export default FormStep;
