import { useFormikContext } from 'formik';

const Step2 = () => {
  const { values, handleChange, errors } = useFormikContext();

  return (
    <>
      <pre>
        Values:
        {JSON.stringify(values, null, 2)}
        <br/>
        Errors:
        {JSON.stringify(errors, null, 2)}
      </pre>
      {/*@ts-ignore*/}
      <input type="checkbox" name="signed" checked={values.signed} onChange={handleChange} />
    </>
  );
};

export default Step2;
