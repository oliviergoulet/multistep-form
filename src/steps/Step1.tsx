import { useFormikContext } from 'formik';

const Step1 = () => {
  const { values, handleChange, errors } = useFormikContext();

  return (
    <>
      <pre>
        Values:
        {JSON.stringify(values, null, 2)}
        <br/>
        Errors:
        {JSON.stringify(errors, null, 2)}
      </pre>
      {/*@ts-ignore*/}
      <input name="name" value={values.name} onChange={handleChange} />
      {/*@ts-ignore*/}
      <input type="number" name="age" value={values.age} onChange={handleChange} />
    </>
  );
};

export default Step1;
