import React, { useState, createContext } from 'react';
import { FormikValues, FormikConfig, FormikHelpers } from 'formik';
import FormContainer from './FormContainer';
import FormNavigation from './FormNavigation';

interface MultiStepFormProps extends FormikConfig<any> {
  children: React.ReactNode;
}

const MultiStepForm: React.FC<MultiStepFormProps> = ({ children, initialValues, validationSchema, onSubmit }) => {
  const [step, setStep] = useState(0);
  const steps = React.Children.toArray(children) as React.ReactElement[];

  const [snapshot, setSnapshot] = useState(initialValues);

  const currentStep = steps[step];
  const totalSteps = steps.length;
  const isLastStep = step === totalSteps - 1;
  
  const handleSubmit = async (values: FormikValues, actions: FormikHelpers<any>) => {
    alert('handleSubmit\t ' + currentStep.props);
    if (currentStep.props.onSubmit) {
      await currentStep.props.onSubmit(values, actions);
    }

    if (isLastStep) {
      return onSubmit(values, actions);
    } else {
      // Reset the touched fields when navigating.
      actions.setTouched({});
      next(values);
    }
  }

  const next = (values: FormikValues) => {
    setSnapshot({ ...snapshot, [step]: values }); 
    setStep(
      Math.min(step + 1, totalSteps - 1)
    );
  };

  const prev = (values: FormikValues) => {
    setSnapshot({ ...snapshot, [step]: values });
    setStep(
      Math.max(step - 1, 0)
    );
  };

  return (
    <FormContainer onSubmit={handleSubmit} initialValues={snapshot[step]} validationSchema={validationSchema.fields[step]}>
      <MultiStepFormContext.Provider value={{step, next, prev, hasNext: !isLastStep, hasPrev: step <= 0}}>
        {currentStep}
      </MultiStepFormContext.Provider>
    </FormContainer>
  );
}

export default MultiStepForm;

interface MultiStepFormContextValue {
  step: number;
  prev: (values: FormikValues) => void,
  next: (values: FormikValues) => void,
  hasNext: boolean,
  hasPrev: boolean,
}

export const MultiStepFormContext = createContext<MultiStepFormContextValue>({ 
  step: 0, 
  prev: () => {},
  next: () => {},
  hasNext: true,
  hasPrev: false,
});
