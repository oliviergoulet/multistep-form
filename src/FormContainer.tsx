/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// A form container is used as an adapter to wrap the form in a Formik
// component.

import { PropsWithChildren } from 'react';
import { Formik, FormikHelpers, FormikValues } from 'formik';

interface FormContainerProps<T> {
  /** The initial values of the form. */
  initialValues: T;
  /** The validation schema of the form. */
  validationSchema?: any;
  /** Whether or not to validate the form on blur. */
  validateOnBlur?: boolean;
  /** Whether or not to validate the form on change. */
  validateOnChange?: boolean;
  /** The event handler for when the user submits the form. 
    * 
    * @param values - The values of the form.
    * @param helpers - The formik helpers.
    */
  onSubmit: (values: T, helpers: FormikHelpers<T>) => void;
}

/** A component which wraps a form element. It is used to provide the form
 * with state management and validation. */
const FormContainer = <T extends FormikValues>({
  initialValues,
  validationSchema,
  validateOnBlur = true,
  validateOnChange = false,
  onSubmit: incompleteHandleSubmit,
  children,
}: PropsWithChildren<FormContainerProps<T>>) => {
  /** 
   * A wrapper for the form's submit handler. This wrapper prevents the form
   * from being submitted while in preview mode.
   */
  const handleSubmit = (values: T, helpers: FormikHelpers<T>): void => {
    incompleteHandleSubmit(values, helpers);
  }

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      validateOnBlur={validateOnBlur}
      validateOnChange={validateOnChange}
      validateOnMount={false}
      onSubmit={handleSubmit}
      enableReinitialize
    >
      {children}
    </Formik>
  );
};

export default FormContainer;

